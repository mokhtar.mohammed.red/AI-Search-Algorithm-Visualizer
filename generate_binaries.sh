#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "BFS.py" --onefile
cp ./dist/BFS ./AI-Search/Assets/PythonScripts/BFS.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec
#wine pyinstaller "BFS.py" --onefile
#cp ./dist/BFS.exe ./AI-Search/Assets/PythonScripts/BFS.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "DFS.py" --onefile
cp ./dist/DFS ./AI-Search/Assets/PythonScripts/DFS.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec0
#wine pyinstaller "DFS.py" --onefile
#cp ./dist/DFS.exe ./AI-Search/Assets/PythonScripts/DFS.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "UCS.py" --onefile
cp ./dist/UCS ./AI-Search/Assets/PythonScripts/UCS.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec
#wine pyinstaller "UCS.py" --onefile
#cp ./dist/UCS.exe ./AI-Search/Assets/PythonScripts/UCS.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "DepthLimited.py" --onefile
cp ./dist/DepthLimited ./AI-Search/Assets/PythonScripts/DepthLimited.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec
#wine pyinstaller "DepthLimited.py" --onefile
#cp "./dist/Depth Limited.exe" ./AI-Search/Assets/PythonScripts/DepthLimited.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "BestFit.py" --onefile
cp ./dist/BestFit ./AI-Search/Assets/PythonScripts/BestFit.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec
#wine pyinstaller "BestFit.py" --onefile
#cp ./dist/BestFit.exe ./AI-Search/Assets/PythonScripts/BestFit.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "Astar.py" --onefile
cp ./dist/Astar ./AI-Search/Assets/PythonScripts/Astar.elf.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec
#wine pyinstaller "Astar.py" --onefile
#cp ./dist/Astar.exe ./AI-Search/Assets/PythonScripts/Astar.exe.bytes
#rm -r build; rm -r dist; rm -r __pycache__; rm ./*.spec

pyinstaller "IterativeDeepening.py" --onefile
cp ./dist/IterativeDeepening ./AI-Search/Assets/PythonScripts/IterativeDeepening.elf.bytes
