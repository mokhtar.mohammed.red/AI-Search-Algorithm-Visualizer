## Project Description

This project was created for the "Artificial Intelligence" course, which tasked students with the following.

- Module to draw a weighted graph (directed or undirected), with a node designated as a start node and some of nodes as goal nodes. 
- Module to select and apply the AI search Strategy on a given graph problem.The implemented project should contain the two categories of search strategies (Uniformed/Informed).
- Use python as a programming language to implement the search methods, however, you can use any GUI to wrap your application.
